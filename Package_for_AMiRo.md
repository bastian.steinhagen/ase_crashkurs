Pakete für ROS erstellen
========================

Dieses Mini-Tutorial dient als kurze Einführung in ROS.
Gedacht ist diese Einführung für all jene, die bisher noch keine Erfahrungen mit ROS gemacht haben und einen Einstiegspunkt suchen.

## ROS Umgebung auf den NetBoot Rechnern einrichten ##

Damit alle ROS-Befehle, die im Terminal eingegeben werden, auch funktionieren muss zunächst die passende Datei (`setup.bash`) gesourced werden.
Eine genauere Beschreibung befindet sich im offiziellen [ROS-Wiki](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment). 

``` bash
source /opt/ros/kinetic/setup.bash
```

Dieser Befehl muss jedes Mal, wenn ein neues Terminal geöffnet wird ausgeführt werden.
Da dies aber recht umständlich ist, ist es sinnvoll sich einen entsprechenden Eintrag in der eigenen `~/.bashrc` anzulegen (genaueres dazu siehe unten).

## Projektordner anlegen ##

Damit man auch bei mehreren ROS-Projekten nicht den Überblick verliert, sollte man für jedes Projekt einen übergeordneten Ordner anlegen. Dieser kann sich zum Beispiel im home Ordner befinden und nach dem jeweiligen Projekt benannt werden.   

Hier folgt jetzt beispielhaft der Befehl dazu, für diesen Crashkurs muss dieser **nicht** ausgeführt werden.

```bash
cd ~
mkdir Projekt_xyz
```
In diesem Ordner kann dann der catkin_ws Ordner erstellt werden.

>Für diesen Crashkurs wurde bereits ein kompletter Projektordner angelegt, der über Git in das home Verzeichnis geklont werden kann. Der nachfolgenden Punkte können deshalb übersprungen werden. Weiter geht es mit [ROS Package für **AMiRo** erstellen](#Package)

## Catkin Workspace anlegen ##

Als Nächstes wird ein Ordner benötigt, in dem später der zu erstellende Code abgelegt werden kann.
Dieser Ordner wird Catkin-Workspace genannt.
Um einzelne Pakete für ROS zu erstellen wird catkin als Build-System von ROS verwendet (Info siehe [hier](http://wiki.ros.org/catkin/conceptual_overview)).

Die nächsten Befehle legen einen Ordner mit dem Namen `catkin_ws` (der einen Unterordner `src` enthält) in dem eigenen Home-Verzeichnis an.
Danach wird in diesen Ordner gewechselt und dort der Befehl `catkin_make` ausgeführt, der weitere Ordner und Dateien anlegt.
Damit ist dieser Ordner jetzt ein gültiger catkin-Workspace.

``` bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/
catkin_make
```

Damit ROS diesen Ordner als aktuellen Catkin-Workspace erkennt, muss hier auch eine entsprechende Setup-Datei gesourced werden:

``` bash
source devel/setup.bash
```

## AmiRo repository in den catkin workspace einbinden

Es gibt zwei Möglichkeiten, sich das Repository bereit zu stellen. 
Im Folgenden werden beide Möglichkeiten erklärt.

#### 1. Nur clonen
In den Workspace Ordner navigieren und dann mit git clonen:

``` bash
cd ~/catkin_ws/src
git clone https://github.com/autonomoussystemsengineering/amiro_robot
cd amiro_robot
git submodule init && git submodule update
```

#### 2. In den ASE-Projektordner clonen
Mit `cd` in den Ordner navigieren, in dem das ASE-Crashkurs Repository mit Git geklont wurde

````bash
cd ase_crashkurs/
git submodule init && git submodule update
cd catkin_ws/src/amiro_robot
git submodule init && git submodule update
````
Aus diese Weise befindet sich alles übersichtlich in nur einem Ordner und kommt sich nicht mit anderen Projekten in die Quere

#### Testen
Indem man folgenden Befehl ausführt, kann getestet werden, ob der Ordner erfolgreich eingebunden wurde:
``` bash
cd ~/catkin_ws
catkin_make
roslaunch amiro_gazebo amiro_world.launch
```


## <a name="Package"></a>ROS Package für **AMiRo** erstellen ##

Der genaue Ablauf (mit Hintergrund-Informationen) ist [hier](http://wiki.ros.org/ROS/Tutorials/CreatingPackage) nachzulesen.

>Ein Package dient in ROS dazu, Code modular aufzubauen und je nach Bedarf zu laden. Es hat sich bewehrt, dass speziell für einzelne unabhängige Aufgaben des ASE Projekts jeweils unterschiedliche Packages erstellt werden.  
So kann für jede Aufgabe einfach das jeweilige Package ausgeführt werden und man kann verschiedene Versionen unabhängig voneinander weiterentwickeln. Hat man einen Aufgabenteil bereits abgeschlossen, können Code-Elemente in ein neues Package übernommen und aufgabenspeziefisch abgeändert werden. So bleibt der vorherige Code unangetastet und funktioniert weiterhin.

Um ein Paket zu erstellen wechseln wir in den `src` Ordner in unserem Catkin-Workspace `cd ~/ase_crashkurs/catkin_ws/src` und führen folgenden Befehl aus:

``` bash
catkin_create_pkg obstacle_reaction amiro_msgs amiro_gazebo
```

Dieser Befehl legt ein neues Paket mit dem Namen `obstacle_reaction` an, welches von den Paketen `std_msgs` und `roscpp` abhängig ist.
Anschließend kann das Paket gebaut werden:

``` bash
cd ~/catkin_ws
catkin_make
source ~/catkin_ws/devel/setup.bash
```

## Alias in der .bashrc anlegen ##

Damit alle Befehle nicht bei jedem neuen Terminal eingegeben werden müssen, bietet es sich an, ein alias Befehl in der `.bashrc` anzulegen.

``` bash
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
echo "alias ros='cd ~/catkin_ws && source devel/setup.bash'" >> ~/.bashrc
source ~/.bashrc
```

Jetzt kann mit dem Befehl `ros` direkt in den Catkin-Ordner gewechselt werden.
Zusätzlich werden auch alle nötigen Setup-Dateien gesourced.

## QtCreator als IDE für ROS einrichten ##

Zum Einstieg ist es leichter eine passend eingerichtete IDE zur Programmierung in ROS einzusetzen, darum wird hier die Einrichtung von QtCreator beschrieben.
Die genaue Beschreibung findet sich [hier](https://wiki.ros.org/IDEs#QtCreator) im ROS-Wiki.
Bevor QtCreator verwendet werden kann, muss schon ein catkin-Workspace existieren und mindestens einmal ein `catkin_make` durchgelaufen sein.

Zunächst ist es wichtig, dass QtCreator aus einem Terminal (in dem alle Files schon gesourced wurden) gestartet wird.
Ein passender Starter für die NetBoot Maschienen befindet sich in diesem Repository (`qtcreator.desktop`).
Neben dem Starter muss auch die `CMakeLists.txt` im Catkin Workspace veränder werden.
Der Einfachheit halber liegt hier ein passendes Skript für diese Aufgabe bereit:

```shell
./qt_creator_setup.sh
```
Jetzt kann QT Creator über den Starter (entweder in diesem Ordner oder jetzt auch auf dem Desktop) geöffnet werden.
Danach kann auf der Willkommen-Seite 'Open Project' ausgewählt werden.

![Open Project](./images/qt_open.png "Qt Projekt öffnen")

Als Projektdatei wird dann die `CMakeLists.txt` aus dem Ordner `catkin_ws/src/` geöffnet.

![Open Cmake](./images/qt_openProject.png "CMakeLists öffnen")

Danach muss der passende `build` Ordner ausgewählt werden (`catkin_ws/build`).

![Select Build](./images/qt_selectBuild.png "Build Ordner auswählen")

Anschließend wird noch das Argument `-DCMAKE_BUILD_TYPE=Debug` der Konfiguration übergeben und es kann auf den `Run CMake` Button gedrückt werden, danach wird der Vorgang mit `Finish` abgeschlossen.

![Run CMake](./images/qt_cmake.png "Run CMake")

Jetzt sollte das Projekt passend konfiguriert sein, um mit ROS arbeiten zu können.

Wenn ein neues Package angelegt wurde sollte nochmals `Run CMake` im QT Creator aufgeführt werden.
Das geht über: Build --> Run CMake

## KDevelop als Alternative ##

Auf den NetBoot Rechnern ist auch die IDE KDevelop installiert welche sich als Alternative zum QT Creator eignet.
Allerdings haben wir dies noch nicht ausführlich genug getestet.

Auch hier muss zunächst der beiliegende Starter aus diesem Ordner auf den Desktop kopiert werden (kann aber auch hier direkt gestartet werden).
Anschließend muss ein vorhandenes Projekt geöffnet werden.
Als Projektdatei wird hier auch wieder die `CMakeLists.txt` im Ordner `catkin_ws/src` ausgewählt.

![Open KDevelop](images/kdev_open.png "KDevelop öffneen und vorhandenes Projekt auswählen")

![KDevelop Settings](images/kdev_setting.png "Einstellungen so belassen")

![KDevelop Settings 2](images/kdev_setting2.png "Auch hier die Einstellungen so belassen")



## Pub/Sub Beispiel ##

Ein Beispiel für einen einfachen Publisher und Subscriber findet sich [hier](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29).
