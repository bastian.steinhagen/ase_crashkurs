#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "amiro_msgs/UInt16MultiArrayStamped.h"

#define SENSOR_OBSTACLE_THRESHOLD 30000
#define SENSOR_FREE 4000
#define SENSOR_NOT_FREE 10000

#define SENSOR_FRONT_LEFT 3
#define SENSOR_FRONT_RIGHT 4
#define SENSOR_BACK_LEFT 0
#define SENSOR_BACK_RIGHT 7

// Arrays zum speichern der Sensorwerte
uint16_t floor_sensor_values[4];
uint16_t prox_sensor_values[8];

// Demo Programm
#define FAHREN 1
#define REAGIEREN 2
#define ANDERS_REAGIEREN 3

#define FREI SENSOR_FREE
#define HINDERNIS SENSOR_OBSTACLE_THRESHOLD

int zustand = FAHREN;
int next_zustand = FAHREN;


int print_sensors(const uint16_t *values) {
  // Sensorwerte ausgeben um Threshold zu bestimmen
  ROS_INFO("Sensorwerte: [%i, %i, %i, %i, %i, %i, %i, %i]", 
	   (int) values[0], 
	   values[1], 
	   values[2], 
	   values[3], 
	   values[4], 
	   values[5], 
	   values[6], 
	   values[7]);
  
  return 0;

}  
    
void floorSensorCallback(const amiro_msgs::UInt16MultiArrayStamped msg)
{
    for(int i = 0; i < 4; i++)
    {
        floor_sensor_values[i] = msg.array.data[i];
    }
}

void proxSensorCallback(const amiro_msgs::UInt16MultiArrayStamped msg)
{
    for(int i = 0; i < 8; i++)
    {
        prox_sensor_values[i] = msg.array.data[i];
    }
}

int checkSensors()
{
  
}

geometry_msgs::Twist forward_left() 
{
    geometry_msgs::Twist twist;
    twist.linear.x = 0.05;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.3;
    
    return twist;
}

geometry_msgs::Twist right() 
{
    geometry_msgs::Twist twist;
    twist.linear.x = 0.016;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = -0.3;
    
    return twist;
}

geometry_msgs::Twist forward() 
{
    geometry_msgs::Twist twist;
    twist.linear.x = 0.08;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.0;
    
    return twist;
}

geometry_msgs::Twist backward() 
{
    geometry_msgs::Twist twist;
    twist.linear.x = -0.08;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.0;
    
    return twist;
}


int main(int argc, char **argv)
{

    ros::init(argc, argv, "obstacle_reaction");
    ros::Time::init();
    ros::Rate rate(49);
    ros::NodeHandle n;

    // Subscriber, die jeden Zyklus die Sensorwerte in die dafür vorgesehenen Arrays schreiben
    ros::Subscriber floor_sub = n.subscribe("/amiro1/proximity_floor/values", 1, floorSensorCallback);
    ros::Subscriber proximity_sub = n.subscribe("/amiro1/proximity_ring/values", 1, proxSensorCallback);
    
    // Publisher, der das Fahrkommando sendet
    ros::Publisher drive_pub = n.advertise<geometry_msgs::Twist>("/amiro1/cmd_vel", 1);
    
    while(ros::ok())
    {
    
        // Am Anfang sollten alle Fahrkommandos auf 0 gestellt werden,
        // damit keine alten Kommandos vom vorherigen Zyklus die neuen 
        // überlagern können
        geometry_msgs::Twist twist;
        
	switch(zustand)
	    {
		case FAHREN:
		  twist = forward(); // Fahren();
		  if((prox_sensor_values[SENSOR_FRONT_LEFT] >= HINDERNIS) || (prox_sensor_values[SENSOR_FRONT_RIGHT] >= HINDERNIS))
		  {
		      next_zustand = REAGIEREN;
		  }
		  else if((prox_sensor_values[SENSOR_BACK_LEFT] >= HINDERNIS) || (prox_sensor_values[SENSOR_BACK_RIGHT] >= HINDERNIS))
		  {
		      next_zustand = ANDERS_REAGIEREN;
		  }
		  break;

		case REAGIEREN:
		  twist = backward(); // Reagieren();
		  if((prox_sensor_values[SENSOR_FRONT_LEFT] <= FREI) || (prox_sensor_values[SENSOR_FRONT_RIGHT] <= FREI))
		  {
		      next_zustand = FAHREN;
		  }
		  break;

		case ANDERS_REAGIEREN:
		  twist = forward_left(); // AndersReagieren();
		  if((prox_sensor_values[SENSOR_BACK_LEFT] <= FREI) || (prox_sensor_values[SENSOR_BACK_RIGHT] <= FREI))
		  {
		      next_zustand = FAHREN;
		  }
		  break;
	    }

    zustand = next_zustand;

    
	
        
        // Am Ende jedes Zyklus wird das Fahrkommando einmal gepublished
        drive_pub.publish(twist);
    
        // Es wird auf das Ende des aktuellen Zyklus gewartet
        rate.sleep();
    
        // Der nächste Zyklus wird begonnen
        ros::spinOnce();

    }
}
 
