Programmierung des echten AMiRo
========================

## Installation / Download

Als Erstes muss der Sourcecode für den AMiRo geladen und installiert werden.
Eine genaue Schritt für Schritt Anleitung wird in der ASE Übung besprochen.
Für eine einfache und schnelle Installation kann das beiliegende Skript verwendet werden.

```shell
./install_real_amiro.sh
```

Dieses Skript benötigt auf den NetBoot Maschienen einige Zeit um durchzulaufen.
Wenn alles geklappt hat, wurde der Ordner `real_amiro` mit allen nötigen Ordnern befüllt.

In diesem Ordner ist auch der Ordner `amiro-os` mit dem AMiRo Grundsystem, welches im weiteren Verlauf bearbeitet werden muss.

## Einrichtung des QT Creator

Auf den NetBoot Maschienen ist der QT Creator schon installiert.
Dieser kann entsprechend eingerichtet werden so, dass der AMiRo Code konfortabel bearbeitet werden kann.

Als erstes wird der QT Creator geöffnet, anschließend wird auf `New Project` geklickt:

![Amiro-OS QTCreator](images/qt_amiro_os_1.png)

Danach `Import Project` und anschließend auf `Import Existing Project` klicken.

![Amiro-OS QTCreator](images/qt_amiro_os_2.png)

Als Nächstes kann der Ordner `amiro-os` ausgewählt werden.

![Amiro-OS QTCreator](images/qt_amiro_os_3.png)

![Amiro-OS QTCreator](images/qt_amiro_os_4.png)

Hier können die Default-Einstellungen in der Regel so belassen werden.

![Amiro-OS QTCreator](images/qt_amiro_os_5.png)

Zum Schluss hier in beiden Feldern `<None>` auswählen und auf `Finish` klicken.

![Amiro-OS QTCreator](images/qt_amiro_os_6.png)

Jetzt ist ein Passendes Projekt für den QT Creator angelegt.

![Amiro-OS QTCreator](images/qt_amiro_os_7.png)