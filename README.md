# ASE_Crashkurs

[tiny.cc/ase_crashkurs](https://gitlab.ub.uni-bielefeld.de/bastian.steinhagen/ase_crashkurs)

Dieser ASE Crashkurs ist von Studenten für Studenten.
Er richtet sich an Programmieranfänger und soll als Basis für einen besseren Einstieg in das Projekt dienen.

Es sind folgende Inhalte vorhanden:

 * Einrichten der Entwicklungsumgebung
   * `Package_for_AMiRo.md`: Einrichtung ROS, anlegen eines Paketes und Nutzung einer IDE
     * `qt_creator_setup.sh`: Setup Skript für den QT Creator
   * `use_amiro_os.md`: Einrichtung und Installation für den echten AMiRo, Nutzung einer IDE
     * `install_real_amiro.sh`: Installationsskript für den echten AMiRo
 * Programmieren in ROS
   * `ROS_Guide.md`: Einstieg ROS mit dem AMiRo und Vorbereitung für Portierung
 * Programmieren des echten AMiRo
   * `Real_AMiRo_Guide.md`: Portierung des ROS Codes auf den echten AMiRo
 * Hilfreiche Zusatzinformationen
   * `Tips_n_Tricks.md`: Sammlung von nützlichen Tipps
