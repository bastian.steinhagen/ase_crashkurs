#!/bin/bash

#############
# FUNCTIONS #
#############

# print an error
function printError {
  echo "$(tput setaf 1)>>> ERROR: $1$(tput sgr 0)" 
}

# print a warning
function printWarning {
  echo "$(tput setaf 3)>>> WARNING: $1$(tput sgr 0)" 
}

# print an information text
function printInfo {
  echo "$(tput setaf 4)>>> INFO: $1$(tput sgr 0)" 
}

### set some variables before we start
printInfo "setting temporary variables..." 
# the directory where this script is executed
EXEC_DIR="${PWD}/real_amiro"
# installation directory which is in the PATH variable
INSTALL_DIR="${PWD}/real_amiro"
# directory where the big binaries go
PROG_DIR="/opt" 

### arm-gnu-none-eabi
printInfo "installing arm-gnu-none-eabi..." 
ARM_GCC_LOCATION="https://launchpad.net/gcc-arm-embedded/4.8/4.8-2014-q1-update/+download/gcc-arm-none-eabi-4_8-2014q1-20140314-linux.tar.bz2" 
wget ${ARM_GCC_LOCATION}
ARM_GCC_TARBALL=`basename ${ARM_GCC_LOCATION}`
tar -jxf ${ARM_GCC_TARBALL}
COMPILER_DIR=`tar --bzip2 -tf ${ARM_GCC_TARBALL} | sed -e 's@/.*@@' | uniq`
mv "${COMPILER_DIR}" ${INSTALL_DIR}
rm ${ARM_GCC_TARBALL}

printf "\n" >> ${HOME}/.bashrc
printf "##### AMiRo ENVIRONMENT CONFIGURATION FOR ASE CRASHCOURSE #####\n" >> ${HOME}/.bashrc
printf "# DO NOT EDIT THESE LINES MANUALLY!\n" >> ${HOME}/.bashrc
printf "export PATH=\$PATH:${HOME}/ase_crashkurs/real_amiro/gcc-arm-none-eabi-4_8-2014q1/\n" >> ${HOME}/.bashrc
printf "export PATH=\$PATH:${HOME}/ase_crashkurs/real_amiro/gcc-arm-none-eabi-4_8-2014q1\n" >> ${HOME}/.bashrc
printf "##### AMiRo ENVIRONMENT CONFIGURATION FOR ASE CRASHCOURSE #####\n" >> ${HOME}/.bashrc

unset ARM_GCC_LOCATION
unset ARM_GCC_TARBALL
unset COMPILER_DIR


### AMiRo-BLT
printInfo "cloning AMiRo-BLT repository to ${INSTALL_DIR}/amiro-blt..." 
cd ${INSTALL_DIR}
git clone http://openresearch.cit-ec.de/git/amiro-os.amiro-blt.git -b 1.0_stable amiro-blt
cd ${EXEC_DIR}

### AMiRo-OS
#printInfo "cloning AMiRo-OS repository to ${INSTALL_DIR}/amiro-os..." 
#cd ${INSTALL_DIR}
#git clone http://openresearch.cit-ec.de/git/amiro-os.amiro-os.git -b 1.0_stable amiro-os
#cd ${EXEC_DIR}

### ChibiOS
printInfo "cloning ChibiOS repository to ${INSTALL_DIR}/ChibiOS..." 
cd ${INSTALL_DIR}
git clone https://github.com/ChibiOS/ChibiOS.git ChibiOS
cd ChibiOS
# git checkout 2e6dfc7364e7551483922ea6afbaea8f3501ab0e
git checkout ver_2.6.8
cd ${EXEC_DIR}

### ChibiOS patches
printInfo "applying custom patches to ChibiOS..." 
cd ${INSTALL_DIR}/ChibiOS
cp ../amiro-os/patches/* ./
for i in `ls | grep patch`; do git am --ignore-space-change --ignore-whitespace < ${i}; done
cd ${EXEC_DIR}

### SerailBoot
printInfo "installing SerialBoot..." 
cd ${INSTALL_DIR}/amiro-blt/Host/Source/SerialBoot
mkdir build
cd build
cmake ..
make
cd ${EXEC_DIR}
