#include "userthread.hpp"

#include "global.hpp"

using namespace amiro;

extern Global global;

//######################################################
// ASE Crashkurs Demo
//######################################################

// Twist Messages ROS like

struct sub_twist_msg
{
    double x;
    double y;
    double z;
};

struct twist_msg
{
    struct sub_twist_msg linear;
    struct sub_twist_msg angular;
};


// Arrays zum speichern der Sensorwerte
uint16_t floor_sensor_values[4];
uint16_t prox_sensor_values[8];

// Thresholds
#define PROX_THRESH_HIGH 5094
#define PROX_THRESH_LOW 0


// Demo Programm
#define FAHREN 1
#define REAGIEREN 2
#define ANDERS_REAGIEREN 3

#define SENSOR_OBSTACLE_THRESHOLD 0.3

#define SENSOR_FRONT_LEFT 3
#define SENSOR_FRONT_RIGHT 4
#define SENSOR_BACK_LEFT 0
#define SENSOR_BACK_RIGHT 7

#define FREI SENSOR_OBSTACLE_THRESHOLD
#define HINDERNIS SENSOR_OBSTACLE_THRESHOLD


int zustand = FAHREN;
int next_zustand = FAHREN;

struct twist_msg forward_left()
{
    struct twist_msg twist;
    twist.linear.x = 0.05;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.3;

    return twist;
}

struct twist_msg right()
{
    struct twist_msg twist;
    twist.linear.x = 0.016;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = -0.3;

    return twist;
}

struct twist_msg forward()
{
    struct twist_msg twist;
    twist.linear.x = 0.08;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.0;

    return twist;
}

struct twist_msg backward()
{
    struct twist_msg twist;
    twist.linear.x = -0.08;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.0;

    return twist;
}

// Methode zum Abfragen der Bodensensoren
void floorSensorCallback()
{
    // Jeden Sensor abfragen und speichern
    for(int i = 0; i < 4; i++)
    {
        floor_sensor_values[i] = global.vcnl4020[i].getProximityScaledWoOffset();
    }
}

// Methode zum Abfragen der Näherungssensoren
void proxSensorCallback()
{
    // Jeden Sensor abfragen und speichern
    for (int sensor = 0; sensor < 8; ++sensor)
    {
        prox_sensor_values[sensor] = global.robot.getProximityRingValue(sensor);
    }

    // Sensorwerte normalisieren
    for (int sensor = 0; sensor < 8; ++sensor)
    {
        register uint16_t prox = prox_sensor_values[sensor];

        // Limit an die Maximalgrenze anpassen
        if (prox > PROX_THRESH_HIGH)
            prox = PROX_THRESH_HIGH;

        // Limit an die Minimalgranze anpassen
        else if (prox < PROX_THRESH_LOW)
            prox = PROX_THRESH_LOW;

        // Den Nullpunkt an die Minimalgrenze anpassen
        prox -= PROX_THRESH_LOW;

        // Normalisieren [0, 1]
        prox_sensor_values[sensor] = float(prox) / float(PROX_THRESH_HIGH-PROX_THRESH_LOW);
    }
}

// Fahrkommando senden
void drive_pub(struct twist_msg twist) {
    types::kinematic kin;
    kin.x = (int32_t) (twist.linear.x * 1000000);
    kin.y = 0;
    kin.z = 0;
    kin.w_x = 0;
    kin.w_y = 0;
    kin.w_z = (int32_t) (twist.angular.z * 1000000);

    global.motorcontrol.setTargetSpeed(kin);
}


UserThread::UserThread() :
  chibios_rt::BaseStaticThread<USER_THREAD_STACK_SIZE>()
{
}

UserThread::~UserThread()
{
}

msg_t
UserThread::main()
{
	/*
	 * SETUP
     */
    for (uint8_t led = 0; led < 8; ++led) {
		global.robot.setLightColor(led, Color(Color::BLACK));
    }

    struct twist_msg twist;
    twist.linear.x = 0.0;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = 0.0;

	/*
	 * LOOP
	 */
    while (!this->shouldTerminate())
    {
        // Die Sensorwerte müssen jetzt am Anfang
        // jedes Zyklus abgefragt werden
        proxSensorCallback();
        floorSensorCallback();

        switch(zustand)
        {
        case FAHREN:
          twist = forward(); // Fahren();
          if((prox_sensor_values[SENSOR_FRONT_LEFT] >= HINDERNIS) || (prox_sensor_values[SENSOR_FRONT_RIGHT] >= HINDERNIS))
          {
              next_zustand = REAGIEREN;
          }
          else if((prox_sensor_values[SENSOR_BACK_LEFT] >= HINDERNIS) || (prox_sensor_values[SENSOR_BACK_RIGHT] >= HINDERNIS))
          {
              next_zustand = ANDERS_REAGIEREN;
          }
          break;

        case REAGIEREN:
          twist = backward(); // Reagieren();
          if((prox_sensor_values[SENSOR_FRONT_LEFT] <= FREI) || (prox_sensor_values[SENSOR_FRONT_RIGHT] <= FREI))
          {
              next_zustand = FAHREN;
          }
          break;

        case ANDERS_REAGIEREN:
          twist = forward_left(); // AndersReagieren();
          if((prox_sensor_values[SENSOR_BACK_LEFT] <= FREI) || (prox_sensor_values[SENSOR_BACK_RIGHT] <= FREI))
          {
              next_zustand = FAHREN;
          }
          break;
        }

        zustand = next_zustand;

        // Am Ende jedes Zyklus wird die Methode einmal aufgerufen
        // anstatt des Publishers
        drive_pub(twist);

        // Es wird auf das Ende des aktuellen Zyklus gewartet
        this->sleep(CAN::UPDATE_PERIOD);
    }

  return RDY_OK;
}

