## Dynamischer Speicher bei embedded Systems
Da der AMiRo ein eingebettetes System ist, verfügt er im Vergleich zu einem Laptop oder Desktop PC auch nur über begrenzte Speciherkapazität.
Dadurch muss schon bei der Programmierung auf den Speicherbedarf geachtet werden. Der Kompiler prüft zwar zur Zeit der Kompilierung, ob genügend Speicherplatz vorhanden ist,
durch dynamische Speichergrößen wie zum Beispiel Listen kann der Speicher zur Laufzeit allerdings voll laufen und dann wird unvorhergesehener Weise Speicherplatz genutzt, der eigentlich für das Betriebssystem vorgesehen ist.
Daraus folgt dann ein Systemabsturz, welchen es zu vermeiden gilt.
In der Praxis bedeutet das folgendes:
  * keine Listen verwenden, sondern lieber etwas zu groß dimensionierte Arrays mit definierter Länge
  * Auch jegliche andere dynamischen Speichergrößen vermeiden


## ROS Queue
Wie im ROS-Guide Code sollte darauf geachtet werden, das die Queue mit der Länge `1` im Subscriber angegeben wird:
```
ros::Subscriber floor_sub = n.subscribe("/amiro1/proximity_ring/values", 1, floorSensorCallback);
```

Die Queue ist eine Art Warteschlange, in der alle empfangenen Nachrichten gespeichert werden.
Wenn sie länger als eins ist, dann befinden sich auch ältere Nachrichten in der Warteschlange, die dann zuerst abgearbeitet werden. Da aber immer die aktuellsten Werte abgefragt werden sollen, ist es ratsam die Länge auf eins zu stellen!
In anderen Anwendungen mag es Sinn machen, auch längere Queues zu verwenden, nicht aber im ASE Projekt.

## ROS Fahrkommandos

Damit man es in ROS einfacher hat, kann man sich Methoden für grundlegende Fahrkommandos anlegen. Denkbar wären zum Beispiel folgendes:  
* Geradeaus
* Auf der Stelle rechts/links drehen
* Links-/Rechtskurve fahren
* Anhalten

```C
geometry_msgs::Twist forward_right() 
{
    geometry_msgs::Twist twist;
    twist.linear.x = 0.016;
    twist.linear.y = 0.0;
    twist.linear.z = 0.0;
    twist.angular.x = 0.0;
    twist.angular.y = 0.0;
    twist.angular.z = -0.3;
    
    return twist;
}

```

## ROS Debugging

Um zu wissen, welcher Code genau in der Simulation gerade ausgeführt wird, oder welche Werte bestimmte Variablen oder Sensoren haben, kann im Code eine Textausgabe eingefügt werden. Der Text wird dann in der Konsole ausgegeben, wenn die zugehörige Code-Zeile gerade ausgeführt wird. Dazu wird der befehl folgendermaßen aufgebaut:

```
variable_x = 5.3;
sensor_y = 2658;

ROS_INFO("Dies ist ein Beispieltext.\n Die Variable X hat folgenden Wert: %f und der Sensor Y hat folgenden Wert: %i", variable_x, sensor_y);
```

Die Ausgabe des `ROS_INFO`-Befehls würde dann folgendermaßen aussehen:

```
>> Dies ist ein Beispieltext.
>> Die Variable X hat folgenden Wert: 5.3 und der Sensor Y hat folgenden Wert: 2658
```

Der Text kommt also in `" "` Anführungszeichen und an der Stelle, wo die Werte der Variablen eingefügt werden sollen, wird mit `%` ein Platzhalter eingefügt. Der Buchstabe nach dem `%` gibt den Datentyp an, ein `%i` steht beispielsweise für einen `integer` und ein `%f` für einen `float`-Wert. Das `\n` markiert einen Zeilenumbruch. Die Variablen, die nach dem `string` folgen, werden dann der Reihe nach in die Platzhalter eingefügt.

## State Mashine bauen

Damit der AMiRo die verschiedenen Aufgabenstellungen bewältigen kann, wird er je nach Situation unterschiedliche Verhaltensweisen aufweisen müssen. Soll er während der fahrt beispielsweise Hindernisse erkennen können und dann entsprechend auf diese reagieren, wird der AMiRo entweder im Modus `fahren` oder `reagieren` sein. Im Modus `fahren` fährt er dann mit konstanter Geschwindigkeit geradeaus und fragt ständig die vorderen Sensoren ab. Detektieren diese ein Hindernis, wird der AMiRo in den Modus `reagieren` wechseln. Die verschiedenen Modi nennt man auch `Zustände`. Damit der AMiRo von einem Zustand in den nächsten wechselt, muss immer eine bestimmte Bedingung erfüllt werden. Um vom Zustand `fahren` in den Zustand `reagieren` zu wechseln, müssen die Sensoren ein Hindernis erkennen.  

Der Code des AMiRo's läuft in einer Schleife, das bedeutet er wird andauernd wieder von vorn abgearbeitet. Dabei befindet sich der AMiRo beim ersten Durchlauf in einem vorher angegebenen Zustand und prüft dann während des Durchgangs die jeweiligen Bedingungen, die erfüllt werden müssen, um in einen anderen Zustand zu wechseln. Sind diese Bedingungen erfüllt, wird der Zustand vorgemerkt, der beim nächsten Durchgang eingenommen werden soll. Wird keine Bedingung erfüllt, bleibt der Amiro auch beim nächsten Durchgang im gleichen Zustand.  

![State Mashine](./images/State_Mashine.png "State Mashine")

Eine State-Mashine kann man am einfachsten durch eine `switch/case` Anweisung realisieren. Dabei gibt es eine Steuervariable, die den Zustand angibt. Je nachdem welchen Wert die Steuervariable beinhaltet, wird der jeweilige Fall (`case`) abgearbeitet:

```C
int zustand = 1;
int next_zustand = 1;
    
loop()
{

    switch(zustand)
    {
        case 1:
        Fahren();
        if(sensoren_1 == 1)
        {
            next_zustand = 2;
        }
        else if(sensoren_2 == 1)
        {
            next_zustand = 3;
        }
        break;

        case 2:
        Reagieren();
        if(sensoren_1 == 0)
        {
            next_zustand = 1;
        }
        break;

        case 3:
        AndersReagieren();
        if(sensoren_2 == 0)
        {
            next_zustand = 1;
        }
        break;
    }

    zustand = next_zustand;
}
```

Das `break` ist am Ende jedes `case` notwendig, damit der `switch` Befehl verlassen und mit der nächsten Anweisung weitergemacht wird.

## #defines nutzen

Gerade wenn man mit einer Variable unterschiedliche Zustände repräsentieren will, bietet sich das `Integer`-Format an, mit dem die Zustände einfach durchnummeriert werden. Allerdings wird es auf Dauer sehr unübersichtlich, wenn man fünf verschiedene Zustände abbildet und sich dann die Bedeutung der einzelnen Nummern merken muss.

* 0 -> stopp
* 1 -> fahren
* 2 -> reagieren
* 3 -> anders reagieren
* 4 -> ...

```C
if(zustand == 3)
{
    // reagiere anders
    zustand = 1;
}
```

Es wäre doch viel schöner, wenn man direkt wüsste, für was der Wert steht, den man der Variable `zustand` zuordnet. Hierfür kann man Platzhalter (*defines*) nutzen, die eine bestimmte Zahl repräsentieren. Eine Zahl kann dabei auch durch mehrere *defines* repräsentiert werden. Im folgenden Beispiel wird die `1` durch `FAHREN` und `HINDERNIS` repräsentiert. Man verwendet konventionell nur Großbuchstaben, damit man die *defines* von normalen Variablen unterscheiden kann. Es wird beim Kompilieren auch kein zusätzlicher Speicherplatz verwendet, da die Zahlen direkt in den kompilierten Code eingesetzt werden.

```C
#define FAHREN 1
#define REAGIEREN 2
#define ANDERS_REAGIEREN 3

#define FREI 0
#define HINDERNIS 1

int zustand = FAHREN;
int next_zustand = FAHREN;


loop()
{
    switch(zustand)
    {
        case FAHREN:
        Fahren();
        if(sensoren_1 == HINDERNIS)
        {
            next_zustand = REAGIEREN;
        }
        else if(sensoren_2 == HINDERNIS)
        {
            next_zustand = ANDERS_REAGIEREN;
        }
        break;

        case REAGIEREN:
        Reagieren();
        if(sensoren_1 == FREI)
        {
            next_zustand = FAHREN;
        }
        break;

        case ANDERS_REAGIEREN:
        AndersReagieren();
        if(sensoren_2 == FREI)
        {
            next_zustand = FAHREN;
        }
        break;
    }

    zustand = next_zustand;
}
```

### Sensoren des AMiRo

The AMiRo uses VCNL4020 proximity sensors in its basic version to rectify the environment.
4 sensors pointing the floor and 8 arranged co-circular on the housing.
Here is a brief overview of sensor indices (F:Front, B:Back):

    Top view of the AMiRo ring sensors and their indices:
      _____
     / 3F4 \
    |2     5|
    |1     6|
     \_0B7_/
    
    Top view of the AMiRo floor sensors and their indices:
      _____
     / 3F0 \
    |2     1|
    |       |
     \__B__/


## Echten AMiRo debuggen
Da der echte AMiRo keine `print` Ausgabe ohne angeschlossenes Kabel machen kann, muss man sich anders behelfen, damit man weiß, welchen Code-Abschnitt der AMiRo gerade abarbeitet. Hierfür können die LED's genutzt werden, welche in der leuchtenden Farbe oder auch Anzahl der Lichter variieren kann.

Mit folgendem Beispiel leuchten alle Lichter blau:

```C
global.robot.setLightColor(constants::LightRing::LED_SSW, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_WSW, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_WNW, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_NNW, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_NNE, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_ENE, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_ESE, Color(Color::BLUE));
global.robot.setLightColor(constants::LightRing::LED_SSE, Color(Color::BLUE));

```
Beziehungsweise die Lichter sind aus:

```C
global.robot.setLightColor(constants::LightRing::LED_SSW, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_WSW, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_WNW, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_NNW, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_NNE, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_ENE, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_ESE, Color(Color::BLACK));
global.robot.setLightColor(constants::LightRing::LED_SSE, Color(Color::BLACK));
```