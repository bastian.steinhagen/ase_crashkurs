#!/bin/bash

# Use this QT Creator Starter and put it to the Desktop
cp qtcreator.desktop ~/Desktop


# Copy the global CMakeList
cd catkin_ws/src
mv CMakeLists.txt CMakeLists.txt.old
cp /opt/ros/kinetic/share/catkin/cmake/toplevel.cmake CMakeLists.txt
echo "#Add custom (non compiling) targets so launch scripts and python files show up in QT Creator's project view.
file(GLOB_RECURSE EXTRA_FILES */*)
add_custom_target(${PROJECT_NAME}_OTHER_FILES ALL WORKING_DIRECTORY ${PROJECT_SOURCE_DIR} SOURCES ${EXTRA_FILES})" >> CMakeLists.txt
