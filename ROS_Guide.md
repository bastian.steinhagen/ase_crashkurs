## ROS Code mit Hinblick auf die Portierung schreiben

Damit sich der Code für den AMiRo später schnell und einfach von ROS auf den echten AMiRo portieren lässt, werden alle ROS-speziefischen Befehle in Methoden verpackt, die dann für den eigentlichen Code verwendet werden. Eigentlich sind das auch nur die Subsciber für die Boden- und Näherungssensoren, sowie der Publisher für die Fahrkommandos.

Im folgenden Beispiel ist das Grundgerüst aufgebaut, welches für ROS benötigt wird:

```C
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "amiro_msgs/UInt16MultiArrayStamped.h"

// Arrays zum speichern der Sensorwerte
uint16_t floor_sensor_values[4];
uint16_t prox_sensor_values[8];
    
void floorSensorCallback(const amiro_msgs::UInt16MultiArrayStamped msg)
{
    for(int i = 0; i < 4; i++)
    {
        floor_sensor_values[i] = msg.array.data[i];
    }
}

void proxSensorCallback(const amiro_msgs::UInt16MultiArrayStamped msg)
{
    for(int i = 0; i < 8; i++)
    {
        prox_sensor_values[i] = msg.array.data[i];
    }
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "obstacle_reaction");
    ros::Time::init();
    ros::Rate rate(49);
    ros::NodeHandle n;

    // Subscriber, die jeden Zyklus die Sensorwerte in die dafür vorgesehenen Arrays schreiben
    ros::Subscriber floor_sub = n.subscribe("/amiro1/proximity_ring/values", 1, floorSensorCallback);
    ros::Subscriber proximity_sub = n.subscribe("/amiro1/proximity_floor/values", 1, proxSensorCallback);
    
    // Publisher, der das Fahrkommando sendet
    ros::Publisher drive_pub = n.advertise<geometry_msgs::Twist>("/amiro1/cmd_vel", 1);
    
    while(ros::ok())
    {
    
        // Am Anfang sollten alle Fahrkommandos auf 0 gestellt werden,
        // damit keine alten Kommandos vom vorherigen Zyklus die neuen 
        // überlagern können
        geometry_msgs::Twist twist;
        twist.linear.x = 0.0;
        twist.linear.y = 0.0;
        twist.linear.z = 0.0;
        twist.angular.x = 0.0;
        twist.angular.y = 0.0;
        twist.angular.z = 0.0;
    
        // Hier können die Sensorwerte genutzt werden, 
        // um ein Fahrkommando zu bestimmen
        // 
        // ...
        //
        // Z.B.: Eine Kurve fahren
        twist.linear.x = 0.1;
        twist.angular.z = 0.3;
    
        // Am Ende jedes Zyklus wird das Fahrkommando einmal gepublished
        drive_pub.publish(twist);
    
        // Es wird auf das Ende des aktuellen Zyklus gewartet
        rate.sleep();
    
        // Der nächste Zyklus wird begonnen
        ros::spinOnce();

    }
}
```

## Wie und wo erstelle ich die Datei?

Vorher wurde bereits ein Package angelegt:

``` bash
catkin_create_pkg obstacle_reaction amiro_msgs amiro_gazebo
```

Jetzt kann eine cpp-Datei für das neue Package angelegt werden.
Die muss im `src` Ordner des Packages liegen.

Wenn die Datei fertig ist, muss sie noch in die `CMakeLists.txt` eingetragen werden.

Dazu wird folgendes an die Datei `~/ase_crashkurs/catkin_ws/srs/obstacle_reaction/CMakeLists.txt` angehängt:

```
add_executable(obstacle_reaction src/obstacle_reaction.cpp)
target_link_libraries(obstacle_reaction ${catkin_LIBRARIES})
add_dependencies(obstacle_reaction amiro_msgs)
```
